import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
    verbose: true,
    preset: 'ts-jest',
    testEnvironment: 'node',
    maxWorkers: 1,
};

// eslint-disable-next-line import/no-default-export
export default config;
