import { NextFunction, Request as expressRequest, Response as expressResponse } from 'express';
import { UsersService } from './users.service';

export class UsersController {
    public static handleFetchUsers = async (
        req: expressRequest,
        res: expressResponse,
        next: NextFunction,
    ): Promise<void> => {
        try {
            // TODO: resolve new eslint errors
            // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
            req.log.info('handleFetchUsers');
            const result = await UsersService.fetchUsers();
            res.status(200).json(result);
        } catch (e) {
            next(e);
        }
    };
}
