import { logger } from '../../utilities/logger';

export type User = {
    id: number;
    name: string;
    colors?: string[];
};

export class UsersService {
    public static async fetchUsers(): Promise<User[]> {
        logger.debug('fetching users...');
        return new Promise<User[]>((resolve) => {
            setTimeout(() => {
                resolve([
                    {
                        id: 1,
                        name: 'User 1',
                        colors: ['blue', 'green'],
                    },
                    {
                        id: 3,
                        name: 'User 2',
                    },
                ]);
            }, 300);
        });
    }
}
