## Base Image
FROM node:16.17.1-alpine3.16 AS base
LABEL stage="base"
ENV NODE_ENV=production
RUN mkdir /opt/express-ts-bootstrap
WORKDIR /opt/express-ts-bootstrap
RUN chown -R node:node /opt/express-ts-bootstrap
USER node
COPY --chown=node:node package*.json ./
RUN npm ci --ignore-scripts && npm cache clean --force
ENV PATH=/opt/express-ts-bootstrap/node_modules/.bin:$PATH

## Dev Dependencies Stage
FROM base as devdeps
LABEL stage="devdeps"
ENV NODE_ENV=development
RUN npm install --ignore-scripts
WORKDIR /opt/express-ts-bootstrap/app

## Dev Stage
# This stage only works if the docker-compose.dev.yaml is used, as it relies on volume mounting the source code
# from the host as well as a few anonymous modules for node_module loading purposes
FROM devdeps as dev
LABEL stage="dev"
CMD ["ts-node-dev", "./src/server.ts"]

## Build Stage
FROM devdeps as build
LABEL stage="source"
COPY --chown=node:node . .
RUN npm test -- --verbose=false
RUN tsc --project tsconfig.production.json

## Production Stage
FROM gcr.io/distroless/nodejs:16 as prod
LABEL stage="prod"
ENV NODE_ENV=production
ENV PORT=80
WORKDIR /opt/express-ts-bootstrap/app
COPY --from=base --chown=nonroot:nonroot /opt/express-ts-bootstrap/node_modules ./node_modules
COPY --from=build --chown=nonroot:nonroot /opt/express-ts-bootstrap/app/dist /opt/express-ts-bootstrap/app/package*.json ./
USER nonroot:nonroot
EXPOSE $PORT
CMD ["server.js"]

